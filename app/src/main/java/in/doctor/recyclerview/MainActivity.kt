package `in`.doctor.recyclerview

import Adapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import doctor.recyclerview.ItemDataModels

class MainActivity : AppCompatActivity() {
    private val itemsList = ArrayList<ItemDataModels>()
    lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val data = ArrayList<ItemDataModels>()
        for (i in 1..20) {
            data.add(ItemDataModels(R.drawable.apple, "Fruit " + i))
        }
        // This will pass the ArrayList to our Adapter
        val adapter = Adapter(data)

        // Setting the Adapter with the recyclerview
        recyclerView.adapter = adapter
    }

    }
