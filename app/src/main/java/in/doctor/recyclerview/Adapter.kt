import `in`.doctor.recyclerview.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import doctor.recyclerview.ItemDataModels

class Adapter(private val fruitsList: List<ItemDataModels>): RecyclerView.Adapter<Adapter.FruitsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FruitsViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.list_items, parent,false)
        return FruitsViewHolder(view)
    }

    override fun onBindViewHolder(holder: FruitsViewHolder, position: Int) {
            val FruitsViewModels = fruitsList[position]
                holder.fruitName.text = FruitsViewModels.fruitsName
        holder.image.setImageResource(FruitsViewModels.imgage)
    }

    override fun getItemCount(): Int {
        return fruitsList.size
    }
    class FruitsViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView){
        val fruitName = itemView.findViewById(R.id.fruits_txt) as TextView
        val image = itemView.findViewById(R.id.image_imageView) as ImageView


    }
}